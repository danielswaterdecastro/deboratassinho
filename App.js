/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  View
} from 'react-native';

import Principal from './src/components/Principal'
import Historico from './src/components/Historico'

import { Router, Scene, Stack } from 'react-native-router-flux'


export default class App extends Component {
  render() {
    return (
      <Router>
        <Stack key="root">
          <Scene key="principal" hideNavBar component={ Principal } initial />
          <Scene key="historico" navTransparent navBarButtonColor="#fff" component={ Historico } />
        </Stack>
      </Router>
    );
  }
}

