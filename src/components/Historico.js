/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
  Dimensions
} from 'react-native';

const logo = require('../img/14680533_537359036473075_6655443802793818808_n.jpg');
const header = require('../img/header_historico.png');

const win = Dimensions.get('window');

export default class Principal extends Component {
  render() {
    return (
      <ScrollView style={ estilo.containerPrincipal }>
        <View style={ estilo.topo }>
          <Image resizeMode='stretch' style={ estilo.headerImg } source={ header } />
        </View>
        <View style={ estilo.containerHistorico }>
        
          <Text style={ estilo.cabecalho }>HISTÓRICO</Text>
          <Text style={ estilo.txtHistorico }>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tristique commodo turpis sed sodales.
            Nam hendrerit vitae quam at mollis. Maecenas risus augue, aliquam aliquet tempor venenatis, fermentum eget libero. Aliquam semper quam nec neque mattis congue. Vivamus mollis
            fermentum leo, vitae facilisis mauris gravida quis.{"\n"}{"\n"}
            Praesent non dolor eget erat ullamcorper volutpat. Pellentesque quis nisi mi. Nam ultrices orci est,
            a fringilla eros dictum eu.{"\n"}{"\n"}
            Praesent leo ipsum, congue et congue ac, interdum sit amet ex. Integer quam sapien, ultricies quis ipsum in, vehicula aliquet orci. Nulla nulla tortor, 
            varius at commodo ut, lacinia molestie purus. Aliquam et nisi at ligula vulputate pellentesque ut vitae nibh. Donec ac enim in enim convallis efficitur 
            ac non nisi. Nunc sodales luctus auctor. Duis mollis a mauris sit amet posuere.
            {"\n"}{"\n"}
            Phasellus tempor turpis eget mi tristique
            placerat. Sed interdum elementum dignissim. 
            Cras id tellus non orci dapibus consectetur at non nisi.
            Mauris vel placerat risus. Suspendisse eu imperdiet ipsum, 
            vitae commodo lacus. Nam justo nisi, malesuada quis nisl at, cursus gravida nisi. Maecenas porta viverra velit eget rutrum. Nulla tempor mauris ex, a blandit nisi euismod a. 
            Nulla convallis et justo vitae finibus. Etiam suscipit 
            neque accumsan lacinia vehicula.
          </Text>
          </View>
        </ScrollView>
    );
  }
}

const estilo = StyleSheet.create({
  containerPrincipal: {
    backgroundColor: '#000',
    flex:1
  },
  cabecalho: {
    fontSize: 25,
    color: '#b7b7b7',
    marginBottom: 15
  },
  containerHistorico: {
    padding: 15,
    marginTop: 20,
  },
  txtHistorico: {
    color: '#fff',
    textAlign: 'justify'
  },
  topo: {
    alignItems: 'center',
  },
  headerImg: {
    width: 420,
    height: 200
  }
})