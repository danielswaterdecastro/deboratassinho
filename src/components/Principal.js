/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View,
  ScrollView,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {Actions, Action} from 'react-native-router-flux'

const logo = require('../img/14680533_537359036473075_6655443802793818808_n.jpg')

export default class Principal extends Component {
  render() {
    return (
      <View style={ estilos.containerPrincipal }>

        <View style={ estilos.containerLogo }>
          <Image style={ estilos.logo } source={ logo } />
        </View>

        <View>
          <TouchableOpacity
            style={ estilos.botao }
            onPress={ () => {
              Actions.historico()
            } }
          >
          <Text style={ estilos.txtBotao }>HISTÓRICO</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={ estilos.botao }
          >
          <Text style={ estilos.txtBotao }>FOTOS</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={ estilos.botao }
          >
          <Text style={ estilos.txtBotao }>VÍDEOS</Text>
          
          </TouchableOpacity>

          <TouchableOpacity
            style={ estilos.botao }
          >
          <Text style={ estilos.txtBotao }>AGENDA</Text>
          
          </TouchableOpacity>

          <TouchableOpacity
            style={ estilos.botao }
          >
          <Text style={ estilos.txtBotao }>CONTATO</Text>
          
          </TouchableOpacity>
        </View>

      </View>
    );
  }
}

const estilos = StyleSheet.create({
    containerPrincipal: {
      flex: 1,
      backgroundColor: '#000'
    },
    containerLogo: {
      alignItems: 'center',
    },
    logo:{
      width: 340,
      height: 340,
      borderColor: '#000',
      marginBottom: 15
    },
    botao: {
      padding: 10,
      backgroundColor: '#FFFFFF15',
      borderWidth: 0,
      marginBottom: 5      
    },
    txtBotao:{
      color: '#fff',
      textAlign: 'center',
      fontSize: 10,
    }
})